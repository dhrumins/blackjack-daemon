import java.util.ArrayList;
import java.util.List;


public class BlackJackPlayer implements Comparable<BlackJackPlayer> {
	private List<Character> cards;
	private int total;
	private boolean isDealer;
	private boolean aceWithValue11;
	private BlackJackPlayer splitPlayer;
	
	public BlackJackPlayer(boolean isDealer) {
		cards = new ArrayList<Character>();
		total = 0;
		aceWithValue11 = false;
		this.isDealer = isDealer;
		splitPlayer = null;
	}

	public List<Character> getCards() {
		return this.cards;
	}

	public int getTotal() {
		return this.total;
	}

	public void addCard(char card) {
		cards.add(card);
		this.total += getCardValue(card);
		adjustPlayerTotal();
	}

	public boolean isGameSplittable() {
		if (this.cards.size() == 2) {
			if (this.cards.get(0) == this.cards.get(1)) {
				return true;
			}
		}
		return false;
	}

	public BlackJackPlayer getSplitPlayer() {
		return this.splitPlayer;
	}

	public boolean addSplitPlayer() {
		if (!this.isGameSplittable()) {
			return false;
		}
		this.splitPlayer = new BlackJackPlayer(false);
		char card = this.cards.remove(1);
		this.total -= getCardValue(card);
		this.splitPlayer.addCard(card);
		return true;
	}

	private int getCardValue(char card) {

		int value = 0;
		switch(card) {

		case 'A':
			if (this.total > 10)
				value = 1;
			else {
				value = 11;
				aceWithValue11 = true;
			}
			break;
		case 'K':
			value = 10;
			break;
		case 'Q':
			value = 10;
			break;
		case 'J':
			value = 10;
			break;
		case '0':
			value = 10;
			break;
		case '9':
			value = 9;
			break;
		case '8':
			value = 8;
			break;
		case '7':
			value = 7;
			break;
		case '6':
			value = 6;
			break;
		case '5':
			value = 5;
			break;
		case '4':
			value = 4;
			break;
		case '3':
			value = 3;
			break;
		case '2':
			value = 2;
			break;
		default:
			break;
		}
		return value;
	}

	private void adjustPlayerTotal() {
		/*
		 * There can be just one Ace, if any, with value
		 * of 11 in the player's list of cards. Every other
		 * Ace will be of value 1 and not 11.
		 */
		while (this.total > 21 && aceWithValue11) {
			/*
			 * The total can be adjusted only once.
			 * Once adjusted, the value of every Ace in
			 * the list of player's cards become 1.
			 */
			this.total -= 10;
			aceWithValue11 = false;
		}
	}

	public void displayInitialCards() {
		if (isDealer) {
			System.out.println("\nDealer's cards :");
			System.out.println("----------------------------------");
			this.displayCard(this.cards.get(0));
			System.out.println("[#]");
			System.out.println("----------------------------------\n");
		}
	}
	public void displayPlayerCards() {
		String playerString;
		if (isDealer)
			playerString = "Dealer's";
		else
			playerString = "Player's";

		display(playerString, this);
	}

	public void displayAllCards() {
		displayPlayerCards();
		BlackJackPlayer splitPlayer = this.getSplitPlayer();
		while (splitPlayer != null) {
			display("Split Game", splitPlayer);
			splitPlayer = splitPlayer.getSplitPlayer();
		}
	}

	private void display(String playerString, BlackJackPlayer player) {
		System.out.println("\n" + playerString + " cards :");
		System.out.println("----------------------------------");
		for (Character card : player.cards) {
			player.displayCard(card);
		}
		System.out.println("Total:  " + player.total);
		System.out.println("----------------------------------\n");
	}

	private void displayCard(char card) {
		if (card == '0')
				System.out.print("[10]  ");
			else
				System.out.print("[" + card + "]  ");
	}

	@Override
	public int compareTo(BlackJackPlayer other) {
		int otherTotal = other.getTotal();
		int returnValue = -1;
		BlackJackPlayer currentPlayer = this;
		while (currentPlayer != null) {
			if (currentPlayer.getTotal() <= 21) {
				if (currentPlayer.getTotal() > otherTotal) {
					returnValue = 1;
				} else if (returnValue != 1 && currentPlayer.getTotal() == otherTotal) {
					returnValue = 0;
				}
			}
			currentPlayer = currentPlayer.getSplitPlayer();
		}
		return returnValue;
	}
}
