# BlackJack Daemon Readme

This is a program for a BlackJack game daemon, which allows the user to play BlackJack game with an automated dealer.
User can play as many games as he wants and in each game, gets a chance to either "HIT" a card, or "STAY" with existing cards,
"SPLIT" the game into 2, if possible or end the game.

The cards are drawn from a set of random card deck, containing cards from "ACE" to "TEN" and also including the face cards from 
"JACK" to "KING", in 4 different suites. The usual rules of BlackJack games controls the game and also decides who wins in
the end, or if dealer/player has been busted or if dealer/player has got a "PERFECT BLACKJACK", etc.

An interactive game, which makes the whole experience a real BlackJack game look-alike.