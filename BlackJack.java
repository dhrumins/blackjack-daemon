public class BlackJack {

	private final char[] cards;

	private BlackJack() {
		char[] cards = { 'A','K','Q','J','0','9','8','7','6','5','4','3','2'};
		this.cards = cards;
	}
	
	public static BlackJack startBlackJack() {
		return new BlackJack();
	}

	public void distributeCards(BlackJackPlayer dealer, BlackJackPlayer player) {
		/*
		 * Start with dealer and add one card to his bucket.
		 * Now do the same for the player.
		 * Repeat this step once again for dealer and player.
		 * This is how the cards are distributed initially.
		 */
		dealer.addCard(this.getNextCard());
		player.addCard(this.getNextCard());
		dealer.addCard(this.getNextCard());
		player.addCard(this.getNextCard());
	}

	private char getNextCard() {
		int nextCardNumber = (int)(Math.random() * 13);
		return this.cards[nextCardNumber];
	}

	public boolean isBusted(BlackJackPlayer player) {
		return player.getTotal() > 21;
	}
	
	public boolean isBlackJack(BlackJackPlayer player) {
		return (player.getTotal() == 21 &&
				player.getCards().size() == 2 &&
				player.getCards().contains('A'));
	}
	
	public void hit(BlackJackPlayer player) {
		player.addCard(this.getNextCard());
	}
}
