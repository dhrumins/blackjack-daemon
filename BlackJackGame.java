import java.util.Scanner;


public class BlackJackGame {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		/*
		 * Initialize the board game to play BlackJack
		 */
		initializeGame();

		boolean playGame = true;
		while (playGame) {

			BlackJack game = BlackJack.startBlackJack();
			BlackJackPlayer dealer = new BlackJackPlayer(true);
			BlackJackPlayer player = new BlackJackPlayer(false);

			/*
			 * Initialize the cards for the player and dealer
			 */
			initBlackJack(game, player, dealer);

			/*
			 * Start the interactive BlackJack game with the user and blackJack daemon
			 */
			playBlackJack(scan, game, player, dealer);

			while (true) {
				System.out.println("\n\n   Do you want to play another game?  [YES (y)  for new game, NO (n) to exit) : ");
				String nextGame = scan.next();

				if (nextGame.equalsIgnoreCase("yes") || nextGame.equalsIgnoreCase("y")) {
					playGame = true;
					break;
				} else if (nextGame.equalsIgnoreCase("no") || nextGame.equalsIgnoreCase("n")) {
					playGame = false;
					break;
				} else {
					continue;
				}
			}
		}

		System.out.println("\n\n ****   Thank you for playing  BLACKJACK   ****\n\n");
	}

	private static void initializeGame() {
		System.out.println("              *************************************");
		System.out.println("              *                                   *");
		System.out.print("              * ");
		System.out.println("Welcome to the BlackJack Game!!!  *");
		System.out.println("              *                                   *");
		System.out.println("              *************************************\n");
		
		System.out.println("Lets start playing....");
	}

	private static void initBlackJack(BlackJack game, BlackJackPlayer player, BlackJackPlayer dealer) {
		
		System.out.println("Distributing cards to the dealer and the player....");
		game.distributeCards(dealer, player);
		dealer.displayInitialCards();
	}

	private static void playBlackJack(Scanner scan, BlackJack game, BlackJackPlayer player, BlackJackPlayer dealer) {

		/*
		 * Now the player provides the input to either hit
		 * or stay. For every hit, blackJack.hit() is called
		 * and a new card it provided to the player.
		 * After every hit call, blackJack computes player's
		 * total and determines, if he has a blackJack or if he
		 * is busted and the game ends there.
		 * If player decides to stay, then his total is just compued
		 * and the dealer plays then.
		 */
		boolean isDealerTerminated = false;
		boolean isPlayerTerminated = false;
		if (game.isBlackJack(dealer)) {
			System.out.println("Dealer has got a perfect BlackJack!!!");
			isDealerTerminated = true;
		}

		if (game.isBlackJack(player)) {
			System.out.println("Player has got a perfect BlackJack!!!");
			isPlayerTerminated = true;
		}

		if (isDealerTerminated) {
			dealer.displayPlayerCards();
			player.displayPlayerCards();
			if (isPlayerTerminated)
				System.out.println("Draw, since both Dealer and Player have got a perfect BlackJack!!!");
			else
				System.out.println("Dealer has won the game!!!");

		} else if (isPlayerTerminated) {
			player.displayPlayerCards();
			dealer.displayPlayerCards();
			System.out.println("Player has won the game!!!");
			
		} else {
			BlackJackPlayer currentPlayer = player;
			while (true) {
				currentPlayer.displayPlayerCards();

				if (game.isBusted(currentPlayer)) {
					if (currentPlayer.getSplitPlayer() != null) {
						System.out.println("Current player game has been BUSTED, so switching to the split game!!!!");
						currentPlayer = currentPlayer.getSplitPlayer();
						continue;
					}

					System.out.println("Player has been BUSTED, so Dealer Wins!!!!");
					isPlayerTerminated = true;
					break;
				}

				if (!currentPlayer.isGameSplittable()) {
					System.out.println("Enter your call [HIT (H)  for next card, STAY (S) to stay] : ");
				} else {
					System.out.println("Enter your call [HIT (H)  for next card, STAY (S) to stay and (D) for splitting the game] : ");
				}

				String call = scan.next();

				if (call.equalsIgnoreCase("split") || call.equalsIgnoreCase("d")) {
					boolean splitGame = currentPlayer.addSplitPlayer();
					if (!splitGame) {
						System.out.println("Invalid call option.\n");
					}
					continue;
				}

				if (call.equalsIgnoreCase("stay") || call.equalsIgnoreCase("s")) {
					System.out.println("Player decides to STAY on " + currentPlayer.getTotal() + "\n");
					if (currentPlayer.getSplitPlayer() != null) {
						System.out.println("\nPlay Split Game");
						currentPlayer = currentPlayer.getSplitPlayer();
						continue;
					}
					break;
				}

				if (call.equalsIgnoreCase("hit") || call.equalsIgnoreCase("h")) {
					game.hit(currentPlayer);
				} else {
					System.out.println("Invalid call option.\n");
				}
			}

			/*
			 * Now dealer's turn, if player has not been busted or
			 * hit perfect BlackJack.
			 * Until the dealer's total hits 17, blackJack hits a card
			 * for the dealer, and after every hit, like the player, the
			 * blackJack gamon decides whether dealer has busted or hit a
			 * blackJack
			 */
			if  (!isPlayerTerminated) {
				while(true) {
					dealer.displayPlayerCards();

					if (game.isBusted(dealer)) {
						System.out.println("Dealer has been BUSTED, so Player Wins!!!!");
						isDealerTerminated = true;
						break;
					}

					if (dealer.getTotal() >= 17) {
						System.out.println("Dealer stays at " + dealer.getTotal() + "\n");
						break;
					}

					game.hit(dealer);
				}

				/*
				 * Neither the dealer or the player got a perfect BlackJack
				 * or got busted, so decide the winner depending who has the higer
				 * total
				 */
				if (!isDealerTerminated) {
					System.out.println("\nResult of the game:");
					System.out.println("---------------------");
					dealer.displayPlayerCards();
					player.displayAllCards();
					int result = player.compareTo(dealer); 
					if (result == -1) {
						System.out.println("Dealer has won the game!!!");
					} else if (result == 1) {
						System.out.println("Player has won the game!!!");
					} else {
						System.out.println("Draw, since both Player and the Dealer have the same total of " + player.getTotal() + "!!!");
					}
				}
			}
		}

	}
}
